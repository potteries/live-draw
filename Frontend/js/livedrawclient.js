var liveDrawGroup = {
	paperSize: {//A4
		width: 297,
		height: 210
	},
	styling: { 
		stageSize:{
			width: $(window).width(),
			height: $(window).height()
		},
		borders: 50//min distance from edge of screen (px)
	}
}
liveDrawGroup.stage = initializeStage(liveDrawGroup)

function initializeStage(drawingGroup){

	var stageSize = drawingGroup.styling.stageSize;
	var borders = drawingGroup.styling.borders;
	var paperSize = liveDrawGroup.paperSize;
	

	var scale = (function(){//self executing function to find correct scale
		var scaleX = (stageSize.width - borders * 2) / paperSize.width;
		var scaleY = (stageSize.height - borders * 2) / paperSize.height;
		return scaleX < scaleY ? scaleX : scaleY;
	})();

	var stage = new Kinetic.Stage({
		container: 'canvasContainer',
		width: stageSize.width,
		height: stageSize.height
	});

	var bgLayer = new Kinetic.Layer({id:'background'});
	bgLayer.add(//add background colour
		new Kinetic.Rect({
			x: 0,
			y: 0,
			width: stageSize.width,
			height: stageSize.height,
			fill: 'skyblue'
		})
	);
	//add bgLayer to the stage
	stage.add(bgLayer);


	var fgLayer = new Kinetic.Layer({id: 'foreground'});

	var paper = new Kinetic.Rect({
		id: 'paper',
		x: 0,
		y: 0,
		width: paperSize.width * scale,
		height: paperSize.height * scale,
		cornerRadius: (paperSize.width * scale) * 0.01,
		stroke: 'black',
		strokeWidth: 2,
		fill: 'cornsilk'
	});
	fgLayer.add(paper);//add the paper to the foreground layer



	fgLayer.x(-(paperSize.width * scale) / 2 + stageSize.width / 2);
	fgLayer.y(-(paperSize.height * scale) / 2 + stageSize.height / 2);
	

	var drawingGroup = new Kinetic.Group('drawingGroup');

	//handle the drawing and mousedown stuff here
	var lineArray = [];
	var currentLine = [];
	var mouseDown = false;
	paper.on('mousedown touchstart', function(){
		
		mouseDown = true;
		var mousePos = stage.getPointerPosition();
		mousePos.x = (mousePos.x - fgLayer.x()) / scale;
		mousePos.y = (mousePos.y - fgLayer.y()) / scale;
		currentLine.push(mousePos.x, mousePos.y);//filling current line array
	});

	paper.on('mousemove touchmove', function(){

		if(mouseDown){
			var mousePos = stage.getPointerPosition();
			mousePos.x = (mousePos.x - fgLayer.x()) / scale;
			mousePos.y = (mousePos.y - fgLayer.y()) / scale;
			currentLine.push(mousePos.x, mousePos.y);

			//clear drawing group
			drawingGroup.removeChildren();
			//redraw the lines in the lineArray
			for(var i = 0; i < lineArray.length; i++){//loop through the lineArray
				var points = [];
				for(var k = 0; k < lineArray[i].length; k++){//loop through the individual lines in the array
					points.push(lineArray[i][k] * scale);
				}
				//draw the lines
				drawingGroup.add(
					new Kinetic.Line({
						points: points,
						stroke: 'red'
					})
				);
			}
			var points = [];
			for(var i = 0; i < currentLine.length; i++){
				points.push(currentLine[i] * scale)
			}
			drawingGroup.add(
				new Kinetic.Line({
					points: points,
					stroke: 'red'
				})
			);
			fgLayer.draw();
		}
		
			//draw the lines in the currentlineArray
	});

	//grab the mouse up event (kinetic)
	paper.on('mouseup', function(){
		mouseDown = false;
		lineArray.push(currentLine);
		currentLine = [];
	});

	//kinetics mouseup event seems to fail sometimes, use jquerys too
	$('#appWrapper').mouseup(function(){
		if(mouseDown){
			paper.fire('mouseup');
		}
	});

	fgLayer.add(drawingGroup);

	//move fgLayer to center of screen
	//add fgLayer to the stage
	stage.add(fgLayer);
	
	return stage;
}
